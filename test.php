<!DOCTYPE html>
<html lang="en">

<head>
    <title>Consumer Side</title>

    <!-- Metadata -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Internal dependencies -->
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h2 class="pagehead">Welcome to the call backs page</h2>

        <div class="sessionActiveOrNot">
            <span>Current Status of Session: </span>
            <div class="sessionActiveGreen" id="GreenIsGo" style="visibility:hidden">
                <span class="ActiveSession">Active</span>
                <div class="GreenBlink"></div>
            </div>
            <div class="sessionInactiveRed" id="RedIsNo" style="visibility:visible">
                <span class="InactiveSession">Inactive</span>
                <div class="RedBlink"></div>
            </div>
        </div>

        <h3 class="pagesubhead">This page is meant to check the call backs when call being made from the consumer side</h3>
    </div>

    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <div class="readmepopup">
            <div class="container">
                <!-- Trigger the modal with a button -->
                <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Readme</button>

                <!-- Modal -->
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">About this module</h4>
                            </div>

                            <div class="modal-body">
                                <p>This module is meant to check and observe various callbacks being made on agent side with starting to end of the call.</p>

                                <p>When a call is made to agent, staring from the very first step various call backs are made in order to get a successful calling experience.</p>

                                <p>Through this module you will basically get an idea about how the stuff works, what functions are called and how everything is working. Please <b>note</b> that this is just the prototype and many features are being added to it day by day. For now you can broadly see the major call backs happening.</p>

                                <p>The current status button shows you the status of the call in simpe english language.</p>

                                <p>The current method button gives you the information about which is the current callback is being called and which method is allowing you to experience the current feature being used.</p>

                                <p>Press on the start button call and then you can see the being filled with the current status and call being returned.</p>

                                <p>There is call back method sheet is also being built through which you will be able to get the detailed info on callbacks and you can also post your queries and suggestions on improving this module.</p>

                                <p>Hope you find it informative.</p>

                                <p> PS there's no terms and conditions checkbox in this module :).</p>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="startCallDiv">
            <button id="go" class="startCallButton">Start Support</button>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="videoStatusOfCall">
            <span class="videoStatus">Video Status: </span> <span id="videosupported" class="videosupported"></span>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-gl-6 col-md-6 col-sm-6 col-xs-12">
            <div class="textShowCallbacks">
                <span class="firstText">Current Status: </span> <span class="showCallbacks"></span>
            </div>
        </div>
        <div class="col-gl-6 col-md-6 col-sm-6 col-xs-12">
            <div class="textShowCallbacks">
                <span class="firstText">Current method: </span> <span class="currentCallbackMethod"></span>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="endCallDiv">
            <button id='end' class="endCallButton">End Support</button>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-gl-6 col-md-6 col-sm-6 col-xs-12">
            <div class="cobrowseControls">
                <button id='pauseCobrowse'>Pause Cobrowse</button>
            </div>
        </div>
        <div class="col-gl-6 col-md-6 col-sm-6 col-xs-12">
            <div class="cobrowseControls">
                <button id='resumeCobrowse'>Resume Cobrowse</button>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="annotationsDivision">
            <button id='annotationToggle'>Annotation Information</button>
            <div class="annotationsInfo" id="annodiv" style="display:none;">
                <p>
                    Information about annotations
                    <br/>
                    <span class="annotationsStroke"></span>
                    <span class="annotationsOpacity"></span>
                    <span class="annotationsWidth"></span>
                    <span class="annotationsLength"></span>
                </p>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="cobrowseOnlyDivision">
            <button id="cobrowseonly" class="btn btn-primary">Co-browse Only Mode</button>
            <h3 id="short-code"></h3>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="pageBeingShared" style="display:none;">
            <span class="pageSharedMessage">This screen is now being shared</span>
        </div>
    </div>

    <!-- External dependencies -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

    <script src="https://cs-snadeem2.cafex.com:8443/assistserver/sdk/web/consumer/assist.js"></script>

    <!-- Internal dependencies -->
    <script src="js/callbacks.js"></script>
</body>
</html>