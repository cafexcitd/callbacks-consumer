/**
 * Taken out from the PHP file.  Each JS code snippet that was within a
 * separate script tag has been added here within a separate IIFE.  This
 * is because I am not sure why the JS code snippets were in separate
 * script tags in the first place, but just in case there is a valid reason
 * so yeah...
 */

(function() {
    'use strict';

    $('#go').click(function() {
        AssistSDK.startSupport({
            destination: 'agent1',
            videoMode: 'full'
        });
    });

    $('#end').click(function() {
        AssistSDK.endSupport();
    });

    $('#pauseCobrowse').click(function() {
        AssistSDK.pauseCobrowse();

        $('.showCallbacks').text('Cobrowse has been paused');
        $('.currentCallbackMethod').text('AssistSDK.pauseCobrowse');
    });

    $('#annotationToggle').click(function() {
        $('#annodiv').toggle();
    });
})();

(function() {
    'use strict';

    AssistSDK.onInSupport = function() {
        $('.pageBeingShared').css('display', 'block');
    };
})();

(function() {
    'use strict';

    AssistSDK.onCobrowseInactive = function() {
        $('.pageBeingShared').css('display', 'none');
    };
})();

(function() {
    'use strict';

    var requestCobrowse = function() {
        console.log('Co-browse requested');
        // window.displayAlert(translations.awaitingCobrowseResponse, 'alert-info', 'cobrowse-alert');
        AssistAgentSDK.requestScreenShare(); // TODO: Why is this being called from the consumer side? This is an Agent Console side command...
    };
})();

(function() {
    'use strict';

    AssistSDK.onWebcamUseAccepted = function() {
        $('.showCallbacks').text('You granted cam permission');
        $('.currentCallbackMethod').text('AssistSDK.onWebcamUseAccepted');
    };

    AssistSDK.onConnectionEstablished = function() {
        $('.showCallbacks').text('call accepted by the agent');
        $('.currentCallbackMethod').text('AssistSDK.onConnectionEstablished');
        $('#GreenIsGo').css('visibility', 'visible');
        $('#RedIsNo').css('visibility', 'hidden');
    };

    AssistSDK.agentRequestedCobrowse = function() {
        $('shwoCallbacks').text('agent requested cobrowse');
    };

    AssistSDK.onCobrowseActive = function() {
        $('.showCallbacks').text('cobrowse active');
        $('.currentCallbackMethod').text('AssistSDK.onCobrowseActive');
    };

    AssistSDK.onAnnotationAdded = function() {
        $('.showCallbacks').text('agent is adding annotations in page');
        $('.currentCallbackMethod').text('AssistSDK.onAnnotationAdded');
        $('.annotationsInfo').css('display', 'block');

        AssistSDK.onAnnotationAdded = function(annotation) {
            $('.annotationsStroke').text('Stroke type of annotation is: ' + annotation.stroke);
            $('.annotationsOpacity').text('Opacity of annotation is: ' + annotation.strokeOpacity);
            $('.annotationsWidth').text('Width of annotation is: ' + annotation.strokeWidth);
            $('.annotationsLength').text('path array length of annotation is: ' + annotation.points.length);
        };

        AssistSDK.onAnnotationsCleared = function() {
            $('.showCallbacks').text('agent has cleared the annotations in page');
        };
    };

    AssistSDK.onPushRequest = function(allow, deny) {
        var confirmation = confirm('The agent is trying to show a document, would you like to view the same?');

        if (confirmation) {
            allow();

            $('.showCallbacks').text('accepted the document to view');
            $('.currentCallbackMethod').text('AssistSDK.onPushRequest and allow()');

            AssistSDK.onDocumentReceivedSuccess = function() {
                $('.showCallbacks').text('document recieved successfully');
                $('.currentCallbackMethod').text('AssistSDK.onDocumentReceivedSuccess');
            };

            AssistSDK.sharedDocument.onClosed = function() { // Not working !!!!!
                $('.showCallbacks').text('document closed');
                $('.currentCallbackMethod').text('AssistSDK.onPushRequest and deny()');
            };
        } else {
            deny();

            $('.showCallbacks').text('rejected the document');
            $('.currentCallbackMethod').text('AssistSDK.onPushRequest and deny()');
        }
    };

    AssistSDK.onError = function(error) {
        $('.showCallbacks').text('Please check the internet connection as the session is experiencing some connectivity errors');
    };

    AssistSDK.onEndSupport = function() {
        $('.showCallbacks').text('call has ended');
        $('.currentCallbackMethod').text('AssistSDK.onEndSupport');
        $('#GreenIsGo').css('visibility', 'hidden');
        $('#RedIsNo').css('visibility', 'visible');
    };
})();

(function() {
    'use strict';

    var $codeDisplay = $('#short-code');

    $('#cobrowseonly').click(function(e) {
        e.preventDefault();

        // Retrieve a short code
        $.ajax('https://cs-snadeem2.cafex.com:8443/assistserver/shortcode/create', {
            method: 'put',
            dataType: 'json'
        }).done(function(response) {
            // Retrieve session configuration (session token, CID etc.)
            $.get('https://cs-snadeem2.cafex.com:8443/assistserver/shortcode/consumer', {
                appkey: response.shortCode
            }, function(config) {
                // Translate the config retrieved from the server, into
                // Parameters expected by AssistSDK
                var params = {
                    cobrowseOnly: true,
                    correlationId: config.cid,
                    scaleFactor: config.scaleFactor,
                    sessionToken: config['session-token']
                };

                // Start the support session
                AssistSDK.startSupport(params);

                // Display the code for the consumer to read to the agent
                $codeDisplay.text(response.shortCode);
            }, 'json');
        });
    });

    // Configure Assist to automatically approve share requests
    AssistSDK.onScreenshareRequest = function () {
        return true;
    };

    // The $endSupport button will be hidden
    // $endButton.hide().css('display', 'inline');

    // When screenshare is active, toggle the visibility of the 'end' button
})();

(function() {
    'use strict';

    if (AssistSDK.isVideoSupported) {
        $('.videosupported').text('video is supported');
    } else {
        $('.videosupported').text('video is not supported');
    }
})();